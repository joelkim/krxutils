# -*- coding:utf-8 -*-


__all__ = [\
    'DataCategory',
    'InvestorCategory',
    'SectorCategory',
    ]
        

class DataCategory:
    class Meta(type(object)):
        def __new__(cls, name, bases, dict_data):
            data = [\
                ('StockTicker',             'A0011'),
                ('StockTrade',              'A3011'),
                ('StockClose',              'A6011'),
                ('StockMarket',             'A7011'),
                ('StockQuote',              'B6011'),
                ('StockPreQuote',           'B8011'),
                ('StockInvestor',           'C1011'),
                ('SectorInvestor',          'C0011'),
                ('ProgramQuote',            'C3011'),
                ('ProgramNotice',           'C5011'),
                ('ProgramTradeSumTotal',    'J0011'),
            ]
            list_key, list_header = zip(*data)
            dict_kh = dict(zip(list_key, list_header))
            dict_hk = dict(zip(list_header, list_key))
            dict_data['list_key'] = list_key
            dict_data['list_header'] = list_header
            dict_data['dict_kh'] = dict_kh
            dict_data['dict_hk'] = dict_hk
            for d in data:
                dict_data[d[0]] = d[1] 
                dict_data[d[1]] = d[0] 
            return type.__new__(cls, name, bases, dict_data)
    __metaclass__ = Meta


class InvestorCategory:
    class Meta(type(object)):
        def __new__(cls, name, bases, dict_data):
            data = [\
                ('Securities',              '1000'),
                ('Insurance',               '2000'),
                ('MutualFund',              '3000'),
                ('Bank',                    '4000'),
                ('SavingsLoans ',           '5000'),
                ('PensionFund',             '6000'),
                ('Government',              '7000'),
                ('Individuals',             '8000'),
                ('Foreigners',              '9000'),
            ]
            list_key, list_header = zip(*data)
            dict_kh = dict(zip(list_key, list_header))
            dict_hk = dict(zip(list_header, list_key))
            dict_data['list_key'] = list_key
            dict_data['list_header'] = list_header
            dict_data['dict_kh'] = dict_kh
            dict_data['dict_hk'] = dict_hk
            for d in data:
                dict_data[d[0]] = d[1] 
                dict_data[d[1]] = d[0] 
            return type.__new__(cls, name, bases, dict_data)
    __metaclass__ = Meta



class SectorCategory:
    class Meta(type(object)):
        def __new__(cls, name, bases, dict_data):
            data = [\
                ('KOSPI',                   '001'),
                ('LargeCap',                '002'),
                ('MidCap',                  '003'),
                ('SmallCap',                '004'),
                ('FoodBeverage',            '005'),
                ('TextileApparel',          '006'),
                ('PaperWood',               '007'),
                ('Chemical',                '008'),
                ('Medical',                 '009'),
                ('Mineral',                 '010'),
                ('Metal',                   '011'),
                ('Machinery',               '012'),
                ('Electronics',             '013'),
                ('Precision',               '014'),
                ('TransportEquipment',      '015'),
                ('Distribution',            '016'),
                ('ElectricityGas',          '017'),
                ('Construction',            '018'),
                ('Transport',               '019'),
                ('Communication',           '020'),
                ('FinancialCompany',        '021'),
                ('Bank',                    '022'),
                ('Securities',              '024'),
                ('Insurance',               '025'),
                ('Services',                '026'),
                ('Production',              '027'),
                ('KOSPI200',                '029'),
            ]
            list_key, list_header = zip(*data)
            dict_kh = dict(zip(list_key, list_header))
            dict_hk = dict(zip(list_header, list_key))
            dict_data['list_key'] = list_key
            dict_data['list_header'] = list_header
            dict_data['dict_kh'] = dict_kh
            dict_data['dict_hk'] = dict_hk
            for d in data:
                dict_data[d[0]] = d[1] 
                dict_data[d[1]] = d[0] 
            return type.__new__(cls, name, bases, dict_data)
    __metaclass__ = Meta

