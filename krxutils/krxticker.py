# -*- coding:utf-8 -*-

"""
This module provides utility functions for parsing and converting
ISIN (International Securities Identifying Number) ticker code for KRX.
This code is based on the following document:

http://isin.krx.co.kr/download/rule.hwp

C{get_spec_from_isin} function returns C{Ticker} class object
which contains the specification of the instrument. C{get_dict()}
methods offers the specs in dictionary form.

>>> from atf.utils.krx.krxticker import *
>>> from pprint import pprint
>>> pprint(Ticker('000087').get_dict())
{'derivative_category': None,
 'isin_shortticker': 'A000087',
 'isin_ticker': 'KR7000082008',
 'matdate': None,
 'matday': None,
 'matmonth': None,
 'matyear': None,
 'name': 'HITEJINRO(2PB)',
 'name_ko': u'\ud558\uc774\ud2b8\uc9c4\ub85c2\uc6b0\uc120\uc8fc(\uc2e0\ud615)',
 'security_category': 'stock',
 'strike': None,
 'ticker': None,
 'underlying': None,
 'warrant_issuer_code': None}
>>> pprint(Ticker('KR7005930003').get_dict())
{'derivative_category': None,
 'isin_shortticker': 'A005930',
 'isin_ticker': 'KR7005930003',
 'matdate': None,
 'matday': None,
 'matmonth': None,
 'matyear': None,
 'name': 'SamsungElectronics',
 'name_ko': u'\uc0bc\uc131\uc804\uc790',
 'security_category': 'stock',
 'strike': None,
 'ticker': None,
 'underlying': None,
 'warrant_issuer_code': None}
>>> pprint(get_spec_from_isin('KR7005930003').get_dict())
{'derivative_category': None,
 'isin_shortticker': 'A005930',
 'isin_ticker': 'KR7005930003',
 'matdate': None,
 'matday': None,
 'matmonth': None,
 'matyear': None,
 'name': 'SamsungElectronics',
 'name_ko': u'\uc0bc\uc131\uc804\uc790',
 'security_category': 'stock',
 'strike': None,
 'ticker': None,
 'underlying': None,
 'warrant_issuer_code': None}
>>> pprint(get_spec_from_isin('005930').get_dict())
{'derivative_category': None,
 'isin_shortticker': 'A005930',
 'isin_ticker': 'KR7005930003',
 'matdate': None,
 'matday': None,
 'matmonth': None,
 'matyear': None,
 'name': 'SamsungElectronics',
 'name_ko': u'\uc0bc\uc131\uc804\uc790',
 'security_category': 'stock',
 'strike': None,
 'ticker': None,
 'underlying': None,
 'warrant_issuer_code': None}
>>> pprint(get_spec_from_isin('KR4101F60005').get_dict())
{'derivative_category': 'future',
 'isin_shortticker': '101F6',
 'isin_ticker': 'KR4101F60005',
 'matdate': (2011, 6, 9),
 'matday': 9,
 'matmonth': 6,
 'matyear': 2011,
 'name': '',
 'name_ko': '',
 'security_category': 'future and option',
 'strike': None,
 'ticker': None,
 'underlying': 'kospi200 index',
 'warrant_issuer_code': None}

C{get_ticker_kospifuture}, C{get_ticker_kospioption} functions return
ISIN ticker of future/option of which specifications are matched with
the given arguments.

>>> get_ticker_kospifuture(2011, 6)
'KR4101F60005'
>>> get_ticker_kospioption(2011, 5, 'call', 290)
'KR4201F52901'
"""

import os
import csv
from pprint import pformat
from string import ascii_uppercase
from numpy import floor, int

from krxdate import calc_matdate

__all__ = [\
    'Ticker',
    'SECURITY_CATEGORY',
    'DERIVATIVE_CATEGORY',
    'UNDERLYING',
    'MAT_YEAR',
    'MAT_MONTH',
    'LIST_KRXTICKER1',
    'LIST_KRXTICKER2',
    'get_spec_from_isin',
    'get_ticker_kospifuture',
    'get_ticker_kospioption',
    ]


cur_dir = os.path.dirname(__file__)
data_path = os.path.join(cur_dir, 'krxticker.txt')
krxticker_reader = csv.reader(open(data_path, 'r'))
LIST_KRXTICKER1 = [row for row in krxticker_reader]
LIST_KRXTICKER2 = [r for r in zip(*LIST_KRXTICKER1)]

del krxticker_reader


class Ticker(object):
    """
    This class summarizes the specifications of a single instrument:
     - ticker
     - isin_ticker
     - isin_shortticker
     - security_category
     - derivative_category
     - underlying
     - strike
     - matdate
     - matyear
     - matday
     - warrant_issuer_code

    @var ticker: ticker string in connector-specific system
    @var isin_ticker: ticker string in standard ISIN system
    @var isin_shortticker: ticker string in short-form ISIN system
    @var security_category: security category string
    @var derivative_category: derivative category string. if not exist, None
    @var underlying: ticker string of underlying. if not exist, None
    @var strike: strike (number). if not exist, None
    @var matdate: (year, month, day) tuple of maturity date. if not exist, None
    @var matyear: maturity year (number). if not exist, None
    @var matmonth: maturity month (number). if not exist, None
    @var matday: maturity day (number). if not exist, None
    @var warrant_issuer_code: warrant issuer's code string. if not exist, None

    """

    ticker = None
    isin_ticker = None
    isin_shortticker = None
    security_category = None
    derivative_category = None
    underlying = None
    strike = None
    matdate = None
    matyear = None
    matmonth = None
    matday = None
    warrant_issuer_code = None
    name = None
    name_ko = None

    def __new__(cls, ticker=None):
        if ticker is not None:
            try:
                return get_spec_from_isin(ticker)
            except Exception, e:
                raise ValueError("unknown ticker. Exception:\n%s" % str(e))
        else:
            return object.__new__(cls)

    def get_dict(self):
        """
        return a dictionary of Ticker member attributes
        """
        return {\
            'ticker': self.ticker,
            'isin_ticker': self.isin_ticker,
            'isin_shortticker': self.isin_shortticker,
            'security_category': self.security_category,
            'derivative_category': self.derivative_category,
            'underlying': self.underlying,
            'strike': self.strike,
            'matdate': self.matdate,
            'matyear': self.matyear,
            'matmonth': self.matmonth,
            'matday': self.matday,
            'warrant_issuer_code': self.warrant_issuer_code,
            'name': self.name,
            'name_ko': self.name_ko,
            }

    def __getitem__(self, key):
        return self.get_dict().get(key)

    def __repr__(self):
        output = ['<%s.%s object at 0x%X>' % (self.__class__.__module__,
                                              self.__class__.__name__,
                                              id(self))]
        output.append(pformat(self.get_dict()))
        return "\n".join(output)

# security category code
SECURITY_CATEGORY = {\
    '1': "government bond",
    '2': "municipal bond",
    '3': "government-agency bond",
    '4': "future and option",
    '5': "fund",
    '6': "hybrid",
    '7': "stock",
    '8': "deposit receipt",
    'A': "warrant",
    }

# future and option type code
DERIVATIVE_CATEGORY = {\
    '1': "future",
    '2': "call",
    '3': "put",
    '4': "spread",
    '5': "call on future",
    '6': "put on future",
    '7': "flex future",
    }

# future and option underlying code
UNDERLYING = {'01': 'kospi200 index'}

# future and option maturity year code
MAT_YEAR = {\
    '6': 1996,
    '7': 1997,
    '8': 1998,
    '9': 1999,
    '0': 2000,
    '1': 2001,
    '2': 2002,
    '3': 2003,
    '4': 2004,
    '5': 2005,
    'A': 2006,
    'B': 2007,
    'C': 2008,
    'D': 2009,
    'E': 2010,
    'F': 2011,
    'G': 2012,
    'H': 2013,
    'J': 2014,
    'K': 2015,
    'L': 2016,
    'M': 2017,
    'N': 2018,
    'P': 2019,
    'Q': 2020,
    'R': 2021,
    'S': 2022,
    'T': 2023,
    'V': 2024,
    'W': 2025,
    }

# future and option maturity month code
MAT_MONTH = {\
    '1': 1,
    '2': 2,
    '3': 3,
    '4': 4,
    '5': 5,
    '6': 6,
    '7': 7,
    '8': 8,
    '9': 9,
    'A': 10,
    'B': 11,
    'C': 12,
    }


def get_spec_from_isin(ticker):
    """
    parses and returns a specification from ISIN ticker code
    including shortened code for future, option.

    @param ticker: a ISIN ticker code of a instrument
    @type ticker: string
    @return: specification of the instrument
    @rtype: C{Ticker}
    @raise ValueError: if cannot parse the ticker, raise a ValueError exception
    """
    try:
        ticker = str(ticker).upper()
    except:
        raise ValueError("ticker must a string!")
    N = len(ticker)
    if N == 5:
        return _get_spec_from_isin_standard(convert_shortticker_5(ticker))
    elif N == 6:
        return _get_spec_from_isin_standard(convert_shortticker_6(ticker))
    elif N == 7:
        return _get_spec_from_isin_standard(convert_shortticker_6(ticker[1:]))
    elif N == 8:
        return _get_spec_from_isin_standard(convert_shortticker_8(ticker))
    elif N == 12:
        return _get_spec_from_isin_standard(ticker)
    else:
        raise ValueError("unknown ticker length: ticker = [%s], " + \
                         "ticker length = %d" % +\
                         (ticker, N))


def _checksum(ticker):
    """
    Calculate and return the check digit
    this code is from http://code.activestate.com/recipes/498277/
    """
    isin2 = []
    for char in ticker[:-1]:
        if char.isalpha():
            isin2.append((ascii_uppercase.index(char.upper()) + 9 + 1))
        else:
            isin2.append(char)
    isin2 = ''.join([str(i) for i in isin2])
    even = isin2[::2]
    odd = isin2[1::2]
    if len(isin2) % 2 > 0:
        even = ''.join([str(int(i) * 2) for i in list(even)])
    else:
        odd = ''.join([str(int(i) * 2) for i in list(odd)])
    even_sum = sum([int(i) for i in even])
    odd_sum = sum([int(i) for i in odd])
    mod = (even_sum + odd_sum) % 10
    listcode = list(ticker)
    listcode[-1] = (str(10 - mod))[-1]
    ticker = str("".join(listcode))
    return ticker


def _get_spec_from_isin_standard(ticker):
    """
    internal function for parsing Standard (long) code for Korea ISIN system
    """
    spec = Ticker()

    # isin_ticker
    spec.isin_ticker = ticker
    # length
    if len(ticker) != 12:
        raise ValueError("ticker length must be 12.")
    # checksum
    ticker2 = _checksum(ticker)
    if ticker2 != ticker:
        raise ValueError("checksum is wrong. " + ticker2 + " is correct.")
    # country
    if ticker[0:2] != 'KR':
        raise ValueError("Not korea ISIN.")
    # security category
    security_category = ticker[2]
    if security_category in SECURITY_CATEGORY.keys():
        spec.security_category = SECURITY_CATEGORY[security_category]

        # future and option
        if spec.security_category == "future and option":
            # derivative category
            derivative_category = ticker[3]
            try:
                spec.derivative_category = \
                    DERIVATIVE_CATEGORY[derivative_category]
            except:
                raise ValueError("unknown derivative category.")
            # underlying
            underlying = ticker[4:6]
            try:
                spec.underlying = UNDERLYING[underlying]
            except:
                raise ValueError("unknown underlying.")
            # mat year
            mat_year = ticker[6]
            try:
                spec.matyear = MAT_YEAR[mat_year]
            except:
                raise ValueError("unknown maturity year.")
            # mat month
            mat_month = ticker[7]
            try:
                spec.matmonth = MAT_MONTH[mat_month]
            except:
                raise ValueError("unknown maturity month.")
            # mat day
            spec.matday = calc_matdate(spec.matyear, spec.matmonth)[2]
            # mat date
            spec.matdate = (spec.matyear, spec.matmonth, spec.matday)
            # strike
            if ['call', 'put'].count(spec.derivative_category):
                spec.strike = get_strike_option_isin(ticker)
            # short code
            if spec.derivative_category == "future":
                spec.isin_shortticker = ticker[3:8]
            elif spec.derivative_category == "call" or \
                 spec.derivative_category == "put":
                spec.isin_shortticker = ticker[3:-1]

        elif spec.security_category == "stock":
            # type
            spec.security_category = "stock"
            # short code
            if ticker in LIST_KRXTICKER2[0]:
                spec.isin_shortticker = 'A' + \
                    LIST_KRXTICKER2[1][LIST_KRXTICKER2[0].index(ticker)]
            else:
                spec.isin_shortticker = 'A' + ticker[3:9]

        elif spec.security_category == "warrant":
            # type
            spec.security_category = "warrant"
            # warrant_issure_code
            spec.warrant_issuer_code = ticker[3:5]
            # underlying
            if ticker[4:5] == '1':
                spec.underlying = 'stock'
            elif ticker[4:5] == '3':
                spec.underlying = 'kospi200 index'
            else:
                raise ValueError("cannot parse warrant ticker")
            # derivative category
            if ticker[5:6] == '1':
                spec.derivative_category = 'call'
            elif ticker[5:6] == '2':
                spec.derivative_category = 'put'
            else:
                raise ValueError("cannot parse warrant ticker")

    else:
        raise ValueError("unknown category.")

    try:
        idx = LIST_KRXTICKER2[0].index(spec.isin_ticker)
        spec.name = LIST_KRXTICKER2[2][idx]
        spec.name_ko = LIST_KRXTICKER2[3][idx].decode('cp949')
    except:
        spec.name = ""
        spec.name_ko = ""
    return spec


def convert_shortticker_5(ticker):
    """
    convert a 5-digit shortened future ticker into a standard ticker
    """
    if len(ticker) != 5:
        raise ValueError("Not short future ticker. length must be 5.")
    if ticker[:3] != '101':
        raise ValueError("Not short future ticker. not start with 101.")
    if MAT_YEAR.has_key(ticker[3]) != 1 or MAT_MONTH.has_key(ticker[4]) != 1:
        raise ValueError("Not short future ticker. unknown year and month.")
    if not ['3', '6', '9', 'C'].count(ticker[4]):
        raise ValueError("Unknown future maturity month!")
    return _checksum('KR4101' + ticker[3:5] + '000' + 'X')


def convert_shortticker_6(ticker):
    """
    convert a 6-digit shortened stock ticker into a standard ticker
    """
    if len(ticker) != 6:
        raise ValueError("Not short stock ticker. length must be 6.")
    if ticker[:2] == '10' and ['03', '06', '09', '12'].count(ticker[-2:]):
        raise ValueError(ticker + " is future code.")
    if ticker in LIST_KRXTICKER2[1]:
        return LIST_KRXTICKER2[0][LIST_KRXTICKER2[1].index(ticker)]
    else:
        return _checksum('KR7' + ticker + '00' + 'X')


def convert_shortticker_8(ticker):
    """
    convert a 8-digit shortened option ticker into a standard ticker
    """
    if len(ticker) != 8:
        raise ValueError("Not short option ticker. length must be 8.")
    if ticker[:3] != '201' and ticker[:3] != '301' :
        raise ValueError("Not short option ticker. not start with 101.")
    if MAT_YEAR.has_key(ticker[3]) != 1 or MAT_MONTH.has_key(ticker[4]) != 1:
        raise ValueError("Not short option ticker. unknown year and month.")
    return _checksum('KR4' + ticker + 'X')


def get_strike_option_isin(ticker):
    """
    validate and calculate strike for future and option
    """
    strikecode = ticker[8:11]
    if strikecode == '000':
        return None
    elif strikecode.isdigit() and ['0', '2', '5', '7'].count(strikecode[2]) == 1:
        return round(int(strikecode) / 2.5) * 2.5
    else:
        raise ValueError(strikecode + " is unknown future option strike ticker.")


def get_ticker_kospifuture(matyear=None, matmonth=None):
    """
    returns isin ticker of a KOSPI index future of which matturity is the given year and date.

    @param matyear: year of maturity
    @type matyear: int
    @param matmonth: month of maturity
    @type matmonth: int {3, 6, 9, 12}
    @return: ticker of the matched KOSPI index future
    @rtype: string
    @raise ValueError: if cannot parse the ticker, raises a ValueError exception
    """
    if matyear is None and matmonth is not None:
        raise ValueError("matyear is None")
    if matyear is not None and matmonth is None:
        raise ValueError("matmonth is None")
    if matyear is None and matmonth is None:
        (matyear, matmonth, matday) = calc_matdate(flag_future=True)
        del matday
    Y = MAT_YEAR.values()
    if Y.count(matyear):
        yearcode = MAT_YEAR.keys()[Y.index(matyear)]
    else:
        raise ValueError("Unknown year!")
    M = MAT_MONTH.values()
    if [3, 6, 9, 12].count(matmonth):
        monthcode = MAT_MONTH.keys()[M.index(matmonth)]
    else:
        raise ValueError("Unknown month!")
    ticker = _checksum('KR4101' + yearcode + monthcode + '000X')
    return ticker


def get_ticker_kospioption(matyear, matmonth, callputtype, strike):
    """
    returns isin ticker of a KOSPI index option of which 
    matturity is the given year and date, call/put type, and strike.

    @param matyear: year of maturity
    @type matyear: int
    @param matmonth: month of maturity
    @type matmonth: int {3, 6, 9, 12}
    @return: ticker of the matched KOSPI index future
    @rtype: string
    @raise ValueError: if cannot parse the ticker, raises a ValueError exception

    """
    if matyear is None and matmonth is not None:
        raise ValueError("matyear is None")
    if matyear is not None and matmonth is None:
        raise ValueError("matmonth is None")
    if matyear is None and matmonth is None:
        (matyear, matmonth, matday) = calc_matdate()
        del matday
    Y = MAT_YEAR.values()
    if Y.count(matyear):
        yearcode = MAT_YEAR.keys()[Y.index(matyear)]
    M = MAT_MONTH.values()
    if M.count(matmonth):
        monthcode = MAT_MONTH.keys()[M.index(matmonth)]
    if ['call', 'c'].count(callputtype):
        typecode = '201'
    elif ['put', 'p'].count(callputtype):
        typecode = '301'
    else:
        raise ValueError(type + " is unknown option type.")
    if strike < 0 or strike > 1000:
        raise ValueError("strike must be positive and less than 1000.")
    strikecode = str(int(floor(round(strike * 4.0 / 10.0) * 10.0 / 4.0)))
    ticker = _checksum('KR4' + typecode + yearcode + monthcode + strikecode + 'X')
    return ticker

if __name__ == "__main__":
    import doctest
    print doctest.testmod()
