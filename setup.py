# -*- coding: utf-8 -*-

from distutils.core import setup

setup(name='krxutils',
      url='http://bitbucket.org/joelkim/krxutils',
      author='Joel Kim (KIM, Doh-Hyoung)',
      author_email='kim.dohhyoung@gmail.com',
      packages=['krxutils'],
      package_data={
        # If any package contains *.txt files, include them:
        '': ['*.txt'],
        },
      zip_safe=False,
)

